#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <ctime>
#include <kalman/ekfilter.hpp>
#include "path.hpp"

#define N 4
#define M 2
#define C 2

class GpsKFilter : public Kalman::EKFilter<double,1> {
public:
    GpsKFilter();
    bool step_cmd(double* z, double* u);
protected:
    void makeA(); // Process / State Jacobian
    void makeH(); // Emission / State Jacobian
    void makeW(); // Process / Measurement Noise Jacobian
    void makeV(); // Emission / Process Noise Jacobian
    void makeQ(); // Process Noise Covariance
    void makeR(); // Measurement Noise Covariance
    void makeProcess();
    void makeMeasure();
};

GpsKFilter::GpsKFilter() {
    setDim(N, M, 2, 2, 2);
    Vector _x(N, 0.0);
    Matrix _P(N, N, 0.0);
    for(size_t i = 1; i <= N; ++i) {
        _P(i, i) = 1.0;
    }
    init(_x, _P);
}

void GpsKFilter::makeA() {
    A(1, 1) = 1.0;
    A(1, 2) = 0.0;
    A(1, 3) = x(4) * cos(x(3));
    A(1, 4) = sin(x(3));
    A(2, 1) = 0.0;
    A(2, 2) = 1.0;
    A(2, 3) = x(4) * (-sin(x(3)));
    A(2, 4) = cos(x(3));
    A(3, 1) = 0.0;
    A(3, 2) = 0.0;
    A(3, 3) = 1.0;
    A(3, 4) = 0.0;
    A(4, 1) = 0.0;
    A(4, 2) = 0.0;
    A(4, 3) = 0.0;
    A(4, 4) = 1.0;
}

void GpsKFilter::makeH() {
    for(size_t i = 1; i <= M; ++i) {
        for(size_t j = 1; j <= N; ++j) {
            H(i, j) = (i == j) ? 1.0 : 0.0;
        }
    }
}

void GpsKFilter::makeW() {
    for(size_t i = 1; i <= N; ++i) {
        for(size_t j = 1; j <= M; ++j) {
            W(i, j) = (i == j) ? 1.0 : 0.0;
        }
    }
}

void GpsKFilter::makeV() {
    for(size_t i = 1; i <= M; ++i) {
        for(size_t j = 1; j <= M; ++j) {
            V(i, j) = (i == j) ? 1.0 : 0.0;
        }
    }
}

void GpsKFilter::makeQ() {
    for(size_t i = 1; i <= M; ++i) {
        for(size_t j = 1; j <= M; ++j) {
            Q(i, j) = (i == j) ? 1.0 : 0.0;
        }
    }
}

void GpsKFilter::makeR() {
    for(size_t i = 1; i <= M; ++i) {
        for(size_t j = 1; j <= M; ++j) {
            R(i, j) = (i == j) ? 1.0 : 0.0;
        }
    }
}

void GpsKFilter::makeProcess() {
    Vector x_(x.size());
    x_(1) = x(1) + (x(4) * sin(x(3)));
    x_(2) = x(2) + (x(4) * cos(x(3)));
    x_(3) = x(3);
    x_(4) = x(4);
    x.swap(x_);
}

void GpsKFilter::makeMeasure() {
    z(1) = x(1);
    z(2) = x(2);
}

bool GpsKFilter::step_cmd(double* z, double* u) {
    Vector _u(C, 0.0), _z(M, 0.0);
    _u(1) = u[0];
    _u(2) = u[1];
    _z(1) = z[0];
    _z(2) = z[1];
    this->step(_u, _z);
    return true;
}

// Main test program for tinyefk.
int main(int argc, const char** argv) {

    int ret = 0;
    const char* path_file_name;
    const char* stats_file_name;
    std::vector<PathNode<double> > path;
    
    // Validate arguments.
    if(argc != 3) {
        std::cout << "Usage: path {path_file} {stats_file}" << std::endl;
        return 1;
    }

    // Get the path file name 
    path_file_name = argv[1]; 
    stats_file_name = argv[2];

    // Read the path from the file.
    ret = read_path(path_file_name, path);
    if(ret != 0) {
      std::cout << "Couldn't read path." << std::endl;
      return ret;
    }
  
    // Apply filter over entire path.
    GpsKFilter filter;
    size_t n   = path.size();
    double err = 0.0;
    std::clock_t start = std::clock();
    for(size_t i = 0; i < n; ++i) {
        PathNode<double> n = path[i];
        if(!filter.step_cmd(n.z, n.u)) {
            std::cout << "Failed." << std::endl;
            return -1;
        }
        double err_i = sqrt(pow(n.x[0] - filter.getX()(1), 2.0) + 
                    pow(n.x[1] - filter.getX()(2), 2.0));
        err += err_i;
    }
    // Clock time.
    double t = (std::clock() - start)/(double)CLOCKS_PER_SEC;
    // Save stats
    ret = save_stats(stats_file_name, n, t, err/n);
    if(ret != 0) {
      std::cout << "Couldn't save performance." << std::endl;
    }
    return ret;
}
