#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <ctime>
#include <TinyEKF.h>
#include "path.hpp"

/**
 * TinyEKF implementation which uses GPS data (easting/northing) as well 
 * as a control input consisting of angular velocity and linear acceleration.
 */
class GpsTinyEKF : public TinyEKF {
protected:
    double u[C]; // Control input.
public:
    GpsTinyEKF() { 
        for(size_t i = 0; i < N; ++i) {
            x[i] = 0.0;
            this->setQ(i, i, 1.0);
            this->setP(i, i, 1.0);
        }
        for(size_t i = 0; i < M; ++i) {
            this->setR(i, i, 1.0);
        }
        for(size_t i = 0; i < C; ++i) {
            u[i] = 0.0;
        }
    }
    ~GpsTinyEKF() { }
    void model(double fx[N], double F[N][N], double hx[M], double H[M][N]);
    bool step_cmd(double* z, double* u);
};

bool GpsTinyEKF::step_cmd(double* z, double* u) {
    this->u[0] = u[0];
    this->u[1] = u[1];
    return this->step(z);
}

void GpsTinyEKF::model(double fx[N], double F[N][N], double hx[M], 
    double H[M][N]) {

    // Predict step.
    fx[0]   = this->x[0] + (this->x[3] * sin(this->x[2])); // easting (m)
    fx[1]   = this->x[1] + (this->x[3] * cos(this->x[2])); // northing (m)
    fx[2]   = this->x[2] +  this->u[0]; // heading (rad)
    fx[3]   = this->x[3] +  this->u[1]; // speed (m/s)

    // Jacobian.
    F[0][0] = 1.0;
    F[0][1] = 0.0;
    F[0][2] = this->x[3] * cos(this->x[2]);
    F[0][3] = sin(this->x[2]);
    F[1][0] = 0.0;
    F[1][1] = 1.0;
    F[1][2] = this->x[3] * (-sin(this->x[2]));
    F[1][3] = cos(this->x[2]);
    F[2][0] = 0.0;
    F[2][1] = 0.0;
    F[2][2] = 1.0;
    F[2][3] = 0.0;
    F[3][0] = 0.0;
    F[3][1] = 0.0;
    F[3][2] = 0.0;
    F[3][3] = 1.0;

    // Measurement step.
    hx[0]   = fx[0];
    hx[1]   = fx[1];

    // Jacobian
    H[0][0] = 1.0;
    H[0][1] = 0.0;
    H[0][2] = 0.0;
    H[0][3] = 0.0;
    H[1][0] = 0.0;
    H[1][1] = 1.0;
    H[1][2] = 0.0;
    H[1][3] = 0.0;
}

// Main test program for tinyefk.
int main(int argc, const char** argv) {

    int ret = 0;
    const char* path_file_name;
    const char* stats_file_name;
    std::vector<PathNode<double> > path;
    
    // Validate arguments.
    if(argc != 3) {
        std::cout << "Usage: path {path_file} {stats_file}" << std::endl;
        return 1;
    }

    // Get the path file name 
    path_file_name = argv[1]; 
    stats_file_name = argv[2];

    // Read the path from the file.
    ret = read_path(path_file_name, path);
    if(ret != 0) {
      std::cout << "Couldn't read path." << std::endl;
      return ret;
    }
  
    // Apply filter over entire path.
    GpsTinyEKF filter;
    size_t n   = path.size();
    double err = 0.0;
    std::clock_t start = std::clock();
    for(size_t i = 0; i < n; ++i) {
        PathNode<double> n = path[i];
        if(!filter.step_cmd(n.z, n.u)) {
            std::cout << "Failed." << std::endl;
            return -1;
        }
        double err_i = sqrt(pow(n.x[0] - filter.getX(0), 2.0) + 
                    pow(n.x[1] - filter.getX(1), 2.0));
        std::cout << "Step: " << i <<  " err = " << err_i << std::endl;
        err += err_i;
    }
    // Clock time.
    double t = (std::clock() - start)/(double)CLOCKS_PER_SEC;

    // Save stats
    ret = save_stats(stats_file_name, n, t, err/n);
    if(ret != 0) {
      std::cout << "Couldn't save performance." << std::endl;
    }
    return ret;
}

