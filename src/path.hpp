#include <iostream>
#include <fstream>
#include <vector>

template<typename T>
class PathNode {
public:
    // Members.
    T x[2], u[2], z[2];
    // Default constructor.
    PathNode()  {
        x[0] = 0.0;
        x[1] = 0.0;
        u[0] = 0.0;
        u[1] = 0.0;
        z[0] = 0.0;
        z[1] = 0.0;
    }
    PathNode(T* x, T* u, T* z) {
        this->x[0] = x[0];
        this->x[1] = x[1];
        this->u[0] = u[0];
        this->u[1] = u[1];
        this->z[0] = z[0];
        this->z[1] = z[1];
    }
    // Copy constructor.
    PathNode(const PathNode<T>& other) {
        this->x[0] = other.x[0];
        this->x[1] = other.x[1];
        this->u[0] = other.u[0];
        this->u[1] = other.u[1];
        this->z[0] = other.z[0];
        this->z[1] = other.z[1];
    }
    // Assignment operator.
    PathNode<T>& operator=(const PathNode<T>& other) {
        this->x[0] = other.x[0];
        this->x[1] = other.x[1];
        this->u[0] = other.u[0];
        this->u[1] = other.u[1];
        this->z[0] = other.z[0];
        this->z[1] = other.z[1];
        return *this;
    }
};


template<typename T>
int read_path(const char* file_name, std::vector<PathNode<T> >& path) {

    int ret = 0;
    std::ifstream ifs;
    PathNode<T> node;

    // Open and read the path file.
    ifs.open(file_name);
    if(!ifs) {
        std::cout << "Couln't open file." << std::endl;
        ret = -1;
        goto cleanup;
    }

    // Read in the file.
    while(ifs >> node.x[0] >> node.x[1]
              >> node.u[0] >> node.u[1]
              >> node.z[0] >> node.z[1]) {
        path.push_back(node);
    }

cleanup:
    // Close the file.
    if(ifs.is_open()) {
      ifs.close();
    }
    return ret;
}

int save_stats(const char* file_name, int n, double t, double err);

