#include <iostream>
#include <fstream>
#include "path.hpp"

int save_stats(const char* file_name, int n, double t, double err) {

    int ret = 0;
    std::ofstream ofs;

    // Open file in append mode.
    ofs.open(file_name, std::ios::app);
    if(!ofs) {
        std::cout << "Couln't open file." << std::endl;
        ret = -1;
        goto cleanup;
    }

    // Write stats to the file.
    ofs << n << " " << t << " " << err << std::endl;

cleanup:
    // Close the file.
    if(ofs.is_open()) {
      ofs.close();
    }
    return ret;
}

