# Final Project Proposal - Kalman Filter Algorithm

  **Name**:  Anthony G. Musco

  **SBID**:  109061296

  **Class**: CSE 591 - Programming Complex Algorithms

## Problem Definition

  Given a series of noisy observations, produce the most likely estimate of the
  current state of the system. This can be accomplished using the well-studied
  Kalman Filter algorithm, which incrementally estimates the state of the
  system and corrects its predictions when new data is observed.


  For my project, I would like to investigate the clarity and complexity of
  the Kalman Filter algorithm, as applied to live GPS data.


  A Kalman Filter is a linear dynamical estimator which produces an estimate of
  the state of one or more random variables given a sequence of observations.
  The Kalman Filter is the time-continous sibling of the Hidden Markov Model, a
  model we've discussed in class.


  A standard Kalman Filter is an inherently incremental algorithm (computes the
  estimation for state X(t) given the previous estimate X(t-1) and observations
  O(t-1)), and thus is perfect for the application of the III method. I will
  develop the first implementation of the algorithm in Python, which will focus
  on clarity and brevity. Next, I will use the III method to develop an
  efficient implementation in C, which will attempt to be as fast as possible.

## Analysis Goals

  The goal for this project will be to develop two implementations: a clear
  implementation in Python, and two efficient implementation in C. The time
  complexities between the two implementations will be compared, and any
  particular optimizations discovered while developing the two implementations
  will be noted.

## Directory Structure

  The project is laid out as follows:

  kalman/
    - bin       -- Generated Object and Executable files.
    - lib       -- Library implementations (kfilter and TinyEkf).
    - data      -- Generated data (random walks).
    - results   -- Performance statistics for each implemenation.
    - img       -- Generated plots and images.
    - python    -- Data model and primary script for python implementation.
    - src       -- C code for the Kalman Filter library implementations.
    Makefile    -- Makefile which compiles the C code (make all).
    README.md   -- This document.

## Running

  Before you begin, you should clean the environment so that fresh data can be
  generated. Next, enter the 'python' directory and enable the virtual
  environment:
  
  ```bash
  make clean
  make reset
  # Project is now clean.
  make
  cd python
  source bin/activate
  # Entered python environment.
  ```

  This should allow you to run the scripts. The primary script, 'k_filter.py',
  contains all the commands you can execute. To generate new data, execute:

  ```bash
  ./k_filter.py gen
  ```

  To run the various implementations, you can execute:

  ```bash
  ./k_filter.py run python    # Slow, clear python implementaion.
  ./k_filter.py run tiny_ekf  # Faster, rudimentary C implementation.
  ./k_filter.py run kfilter   # Template-optimized C++ implementation.
  ./k_filter.py run all       # All implementations.
  ```

  After seach 'run' command, the statistics for each implementation are saved
  to the 'results' directory. You can check in there to see how they performed,
  or run the final command:
  
  ```bash
  ./k_filter.py analyze
  ```
  
  This will display the performance metrics for all three implementations.
