SRC_DIR=src
BIN_DIR=bin
LIB_DIR=lib
FLAGS=-Wall -Werror -g -Wno-error=comment
INCLUDE=-I${LIB_DIR}/TinyEkf -I${LIB_DIR}/kfilter
LD=-lm

TINY_EKF_SRC:=${LIB_DIR}/TinyEkf
KFILTER_SRC:=${LIB_DIR}/kfilter

.PHONY: all
all: ${BIN_DIR}/tiny_ekf ${BIN_DIR}/kfilter

${BIN_DIR}/path.o: ${SRC_DIR}/path.cpp ${SRC_DIR}/path.hpp
		g++ -c ${FLAGS} -std=c++11 ${INCLUDE} -o $@ $<

${BIN_DIR}/kfilter.o: ${SRC_DIR}/kfilter.cpp ${SRC_DIR}/path.hpp
		g++ -c ${FLAGS} -std=c++11 ${INCLUDE} -o $@ $<

${TINY_EKF_SRC}/${BIN_DIR}/tiny_ekf.o: ${TINY_EKF_SRC}/tiny_ekf.c
		gcc -c ${FLAGS} -o $@ $<

${BIN_DIR}/tiny_ekf.o: ${SRC_DIR}/tiny_ekf.cpp ${SRC_DIR}/path.hpp
		g++ -c ${FLAGS} -std=c++11 ${INCLUDE} -o $@ $<

${BIN_DIR}/kfilter: ${KFILTER_SRC}/libkalman.a ${BIN_DIR}/kfilter.o \
										${BIN_DIR}/path.o
		g++ -o $@ ${LD} $^

${BIN_DIR}/tiny_ekf: ${TINY_EKF_SRC}/${BIN_DIR}/tiny_ekf.o \
										 ${BIN_DIR}/tiny_ekf.o ${BIN_DIR}/path.o
		g++ -o $@ ${LD} $^

reset:
		rm -rf data/*
		rm -rf results/*

clean:
		rm -rf ${BIN_DIR}/*
		rm -rf ${TINY_EKF_SRC}/${BIN_DIR}/*
