#!/usr/bin/env python
import sys
import os
from os import path
import pdb
from time import time
from numpy.random import random
from numpy.random import normal
from numpy import linspace
import matplotlib.pyplot as plt
from model import *
from subprocess import call


# Global variables for loading/saving data (with default values).
BASE_DIR    = '../'
DATA_DIR    = '../data'
RESULTS_DIR = '../results'
IMG_DIR     = '../img'


# Parameters for random walk generation.
MAX_SPEED     = 50
MIN_REV_SPEED = 5.0
ANG_START     = 0.30
ANG_STOP      = 0.30
ANG_MAX       = 0.5
LIN_START     = 0.20
LIN_STOP      = 0.40
LIN_MAX       = 1.0
EPS           = 1E-5


def random_command(u, x):
    """ Randomly updates the angular velocity and linear velocity components
    of the command vector 'u'.
    Parameters:
    u -- The current command vector.
    x -- The current state vector.
    """
    should_stop_rev = lambda: x[3,0] < 1.0 and u[1, 0] < 0.0
    # ANGULAR VELOCITY:
    # If the command is zero, randomly generate a new command.
    if (u[0, 0] - 0.0) < EPS:
        if random() < ANG_START:
            u[0, 0] = (random() * 2*ANG_MAX) - ANG_MAX
    # There is a current command, randomly stop it.
    elif random() < ANG_STOP:
        u[0, 0] = 0.0
    # LINEAR ACCELERATION:
    # If the command is zero, randomly generate a new command.
    if (u[1, 0] - 0.0) < EPS:
        if random() < LIN_START:
            u[1, 0] = random() * LIN_MAX
            if random() < 0.5 and x[3, 0] > MIN_REV_SPEED:
                u[1, 0] *= -1.0
    # There is a current command, randomly stop it.
    elif random() < LIN_STOP or should_stop_rev():
        u[1, 0] = 0.0


def random_walk(n):
    """ Generates a random walk of 'n' steps using the parameters above. The
    walk is governed by the control parameters: angular velocity and linear
    acceleration. The control parameters are randomly perturbed in order to
    produce a sufficiently interesting GPS trajectory.
    Parameters:
    n -- The number of steps to walk.
    Returns:
    x -- A column vector containing the true state.
    z -- A column vector containing the noisy measurement.
    """
    # Initialize the data structures for the walk.
    k_filter = GpsKFilter()
    u = GpsKFilter.init_u()
    # Take n steps.
    for i in range(n):
        # Propogate and make measurement.
        random_command(u, k_filter.x)
        z = k_filter.noisy_step(u)
        yield(k_filter.x.copy(), u.copy(), z.copy())


def write_performance(n, t, f_err):
    f_name = path.join(RESULTS_DIR, 'python.txt')
    with open(f_name, 'a') as f:
        f.write('%d %f %f\n' % (n, t, f_err))


def write_walk(n):
    f_name = path.join(DATA_DIR, 'walk-%d.txt' % n)
    with open(f_name, 'w') as f:
        for x, u, z in random_walk(n):
            # Serialize the path.
            f.write('%f %f %f %f %f %f\n' % (x[0,0], x[1,0], u[0,0], \
                u[1, 0], z[0, 0], z[1, 0]))


def read_walks():
    for file_name in os.listdir(DATA_DIR):
        full_path = path.join(DATA_DIR, file_name)
        assert path.exists(full_path), 'Invalid path'
        # Initialize lists.
        x = []; u = []; z = []
        with open(full_path) as f:
            for l in f:
                floats = [float(i) for i in l.strip().split()]
                x.append([floats[0], floats[1]])
                u.append([floats[2], floats[3]])
                z.append([floats[4], floats[5]])
        yield (x, u, z)


def save_current_plot(key):
    """ Saves the current plot with the indicated title.
    Parameters:
    title -- Title to give to the plot.
    """
    title = '_'.join(key.lower().split()) + '.png'
    img_file = path.join(IMG_DIR, title)
    plt.savefig(img_file)


def gen():
    """ Generates a bunch of walks on a log scale."""
    for n in linspace(100, 10000, num=20):
        print('Generating walk of %d steps...' % int(n))
        write_walk(int(n))


def run_base():
    targets = ['all', 'python', 'tiny_ekf', 'kfilter']
    if len(sys.argv) >= 3 and sys.argv[2] in targets:
        if sys.argv[2] in targets:
            { targets[0]: run_all,
              targets[1]: run_python,
              targets[2]: run_tiny_ekf,
              targets[3]: run_kfilter}[sys.argv[2]]()
    else:
        print('Usage: k_filter run [target]')
        print('  - targets: ', targets)


def run_all():
    run_tiny_ekf()
    run_kfilter()
    run_python()


def run_python():
    """ Runs the python implementation of the Kalman Filter."""
    def err(x, y):
        dist = lambda a, b: math.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)
        return sum(dist(i, j) for i, j in zip(x, y))/len(x)
    # Read the walks.
    for x, u, z in read_walks():
        # Apply filter (and time it).
        k_filter = GpsKFilter()
        t    = time()
        filt = k_filter.apply_filter(z, u)
        odom = k_filter.apply_odometry(u)
        t    = time() - t
        # Extract error.
        n        = len(x)
        odom_err = err(x, odom)
        filt_err = err(x, filt)
        # Note performance.
        print('Path len: {} ({} s):'.format(n, t))
        print('  Avg Odometry err: {} m'.format(odom_err))
        print('  Avg Filter err:   {} m'.format(filt_err))
        write_performance(n, t, filt_err)
        # Plot the Points.
        plt.figure()
        plt.scatter(*zip(*x), c='b')
        plt.scatter(*zip(*filt), c='g')
        plt.scatter(*zip(*odom), c='r')
        plt.show()
        save_current_plot('walk-{}'.format(n))


def run_tiny_ekf():
    """ Runs the tinyekf implementation of the Kalman Filter."""
    stats_file = path.join(RESULTS_DIR, 'tinyekf.txt')
    for f in os.listdir(DATA_DIR):
        data_file = path.join(DATA_DIR, f)
        assert path.exists(data_file)
        print('Running tinyekf on ' + f)
        call(['../bin/tiny_ekf', data_file, stats_file])


def run_kfilter():
    """ Runs the kfilter implementation of the Kalman Filter."""
    stats_file = path.join(RESULTS_DIR, 'kfilter.txt')
    for f in os.listdir(DATA_DIR):
        data_file = path.join(DATA_DIR, f)
        assert path.exists(data_file)
        print('Running kfilter on ' + f)
        call(['../bin/kfilter', data_file, stats_file])


def analyze():
    plt.figure()
    plt.title('Running Times')
    plt.xlabel("Path Steps")
    plt.ylabel("Running Time (s)")
    plt.grid()
    settings = {'python':   ('o-', 'r'), 
                'tink_ekf': ('o-', 'b'), 
                'kfilter':  ('o-', 'g')}
    for f in os.listdir(RESULTS_DIR):
        name = f.split('.')[0]
        s = settings[name]
        results_file = path.join(RESULTS_DIR, f)
        assert path.exists(results_file)
        print('Analyzing %s results...' % name)
        with open(results_file) as rf:
            ns = []; ts = []; es = [];
            for l in rf:
                data = l.strip().split()
                ns.append(int(data[0]))
                ts.append(float(data[1]))
                e = float(data[2])
            plt.plot(sorted(ns), sorted(ts), s[0], color=s[1], label=name)
    plt.legend(loc="lower right")
    plt.show()


# Entry point.
if __name__ == "__main__":
    commands = ['gen', 'run', 'analyze']       
    if len(sys.argv) >= 2 and sys.argv[1] in commands:
        { commands[0]: gen,
          commands[1]: run_base,
          commands[2]: analyze }[sys.argv[1]]()
    else:
        print('Usage: k_filter [cmd]')
        print('  - cmd: ', commands)

