import math
import pdb
import numpy as np
from numpy import vstack
from numpy.random import multivariate_normal

PROC_STDV = (1.0, 1.0)
MEAS_STDV = (1.0, 1.0)
 
class GpsKFilter(object):
    """ Kalman filter implementation which models a 4-dimensional state
    (easting, northing, heading, and speed) using GPS data and angular
    velocity, linear acceleration commands."""

    @staticmethod
    def init_x():
        """ State vector.
            (N x 1), where N is the number of state variables.
        """
        return np.matrix([
            [0.0], # easting  (m)
            [0.0], # northing (m)
            [0.0], # heading  (rad)
            [0.0], # speed    (m/s)
        ])

    @staticmethod
    def init_u():
        """ Contol vector.
            (M x 1), where M is the number of control parameters.
        """
        return np.matrix([
            [0.0], # angular velocity (rad/s)
            [0.0], # linear acceleration (m/s^2)
        ])

    @staticmethod
    def init_z():
        """ Observation vector.
            (O x 1), where O is the number of observed variables.
        """
        return np.matrix([
            [0.0], # easting (m)
            [0.0], # northing (m)
        ])

    @staticmethod
    def init_F():
        """ State transition matrix
        (N x N), where N is the number of state variables. Each value (i, j)
        indicates how the new state variable 'i' is influenced by the previous
        state variable 'j' 
        """
        # Will get updated using 'update_F()'
        return np.matrix([
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ])

    @staticmethod
    def update_F(F, x):
        F[0, 3] = math.sin(x[2, 0]) # easting += sin(heading) * speed.
        F[1, 3] = math.cos(x[2, 0]) # northing += cos(heading) * speed.

    @staticmethod
    def init_B():
        """ Control transition matrix
        (N x M), where N is the number of state variables and M is the number of
        control parameters. Each value (i, j) indicates how state variable 'i' is
        incluenced by control parameter 'j'.
        """
        return np.matrix([
            [0.0, 0.0],
            [0.0, 0.0],
            [1.0, 0.0],
            [0.0, 1.0],
        ])

    @staticmethod
    def init_H():
        """ Emission matrix.
        (N x O), where N is the number of state variables and O is the number of
        observed variables.
        """
        return np.matrix([
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, 0.0],
        ])

    @staticmethod
    def init_P():
        """ Covariance matrix
        (N x N), where N is the number of state variables.
        """
        return np.matrix([
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ])

    @staticmethod
    def init_Q():
        """ Process noise matrix.
        (N x N), where N is the number of state variables.
        """
        return np.matrix([
            [PROC_STDV[0]**2, 0.0, 0.0, 0.0],
            [0.0, PROC_STDV[1]**2, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0],
        ])

    @staticmethod
    def init_R():
        """ Measuerement noise matrix.
        (M x M), where N is the number of state variables.
        """
        return np.matrix([
            [MEAS_STDV[0]**2, 0.0],
            [0.0, MEAS_STDV[1]**2],
        ])

    def reset(self):
        """ Resets the filter to it's initial state."""
        self.x = GpsKFilter.init_x()
        self.F = GpsKFilter.init_F()
        self.B = GpsKFilter.init_B()
        self.H = GpsKFilter.init_H()
        self.P = GpsKFilter.init_P()
        self.Q = GpsKFilter.init_Q()
        self.R = GpsKFilter.init_R()

    def __init__(self):
        """ Constructor for this filter."""
        self.reset()

    def multinormal(self, cov):
        mean = np.zeros(cov.shape[0])
        return vstack(multivariate_normal(mean, cov))

    def noisy_step(self, u):
        GpsKFilter.update_F(self.F, self.x)
        w = self.multinormal(self.Q)               # Process Noise.
        x = self.F.dot(self.x) + self.B.dot(u) + w # Noisy propogation
        v = self.multinormal(self.R)               # Observation Noise.
        z = self.H.dot(self.x) + v                 # Noisy observation
        self.x = x
        return z
    
    def filter(self, z, u):
        """ Primary filtering algorithm. This function takes a measurement and
        control input (z and u, respectively), and outputs a corrected state.
        This algorithm was designed to be as clear and concise as possible,
        using the mathematical formulation of the Kalman Filter as defined in
        www.cs.unc.edu/~welch/media/pdf/kalman_intro.pdf
        """
        # Covert data structures.
        z = np.vstack(z)
        u = np.vstack(u)
        
        # Predict
        GpsKFilter.update_F(self.F, self.x)
        x_predict = self.F.dot(self.x) + self.B.dot(u)
        P_predict = self.F.dot(self.P).dot(self.F.transpose()) + self.Q

        # Calculate Kalman Gain.
        H_T   = self.H.transpose()
        S     = self.H.dot(P_predict).dot(H_T) + self.R
        S_inv = np.linalg.inv(S)
        K     = P_predict.dot(H_T).dot(S_inv)

        # Update
        err    = z - self.H.dot(x_predict)
        self.x = x_predict + K.dot(err)
        self.P = (np.eye(self.x.shape[0]) - K.dot(self.H)).dot(P_predict)

        # Return filtered observations.
        z_post = self.H.dot(self.x)
        return [z[i,0] for i in range(z.shape[0])]

    def odometry(self, u):
        """ Generates the expected observation given the current state of the
        system and a command.
        Parameters:
        u -- The command to apply to the system.
        Returns:
        z -- The expected observation.
        """
        GpsKFilter.update_F(self.F, self.x) 
        u = np.vstack(u)
        self.x = self.F.dot(self.x) + self.B.dot(u)
        z = self.H.dot(self.x)
        return [z[i,0] for i in range(z.shape[0])]

    def apply_filter(self, z, u):
        """ Generates a series of corrected observations given a list of real
        obsercations and commands sent to the system.
        Parameters:
        z -- List of real observations made.
        u -- List of commands sent to the system.
        Returns:
        z -- The corrected observation.
        """
        z_filtered = [self.filter(i, j) for i, j in zip(z, u)]
        self.reset()
        return z_filtered

    def apply_odometry(self, u):
        """ Generates a series of expected observations given the raw odometry
        for the system.
        Parameters:
        u -- List of commands sent to the system.
        Returns:
        z -- The expected observation.
        """
        z = [self.odometry(i) for i in u]
        self.reset()
        return z

